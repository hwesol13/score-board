export const DEFAULT_MATCHES_COUNT = 10;

export const MIN_TEAM_NAME_CHARS_COUNT = 5;

export const DEFAULT_TEAM_SCORE = 0;
