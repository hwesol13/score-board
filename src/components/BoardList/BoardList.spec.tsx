import { render, screen } from '@testing-library/react';

import { generateMatches } from '../../utils/matches/matches';

import { BoardList } from './BoardList';
import { LIST_CONTAINER_TEST_ID, LIST_NO_DATA_INFO_TEST_ID } from './testIds';

describe('Component::BoardList', () => {
  describe('Renders with data', () => {
    const matches = generateMatches();

    beforeEach(() => {
      render(
        <BoardList
          matches={matches}
        />
      )
    });

    it('Renders list', () => {
      const listElement = screen.getByTestId(LIST_CONTAINER_TEST_ID);

      expect(listElement).toBeInTheDocument();
    });
  });

  describe('Renders without data', () => {
    beforeEach(() => {
      render(
        <BoardList
          matches={[]}
        />
      )
    });

    it('Renders empty data info', () => {
      const noDataElement = screen.getByTestId(LIST_NO_DATA_INFO_TEST_ID);

      expect(noDataElement).toBeInTheDocument();
    });
  });
});
