import { Match, MatchSide } from '../../types/matches';
import { BoardListItem } from '../BoardListItem';

import { LIST_CONTAINER_TEST_ID, LIST_NO_DATA_INFO_TEST_ID } from './testIds';

type Props = {
  matches: Match[];
  handleFinishMatch?: (matchId: string) => void;
  handleScoreGoal?: (side: MatchSide, matchId: string) => void;
};

export const BoardList = ({ matches, handleFinishMatch, handleScoreGoal }: Props) => {
  const handleScoreHomeGoal = (matchId: string) => {
    if (handleScoreGoal) {
      handleScoreGoal(MatchSide.HOME, matchId);
    }
  };

  const handleScoreAwayGoal = (matchId: string) => {
    if (handleScoreGoal) {
      handleScoreGoal(MatchSide.AWAY, matchId);
    }
  };

  if (!matches?.length) {
    return (
      <p data-testid={LIST_NO_DATA_INFO_TEST_ID}>
        No data
      </p>
    );
  }

  return (
    <div data-testid={LIST_CONTAINER_TEST_ID}>
      {matches.map(({ home, away, id }) => (
        <BoardListItem
          key={id}
          matchId={id}
          homeTeamName={home.name}
          awayTeamName={away.name}
          homeTeamScore={home.score}
          awayTeamScore={away.score}
          handleFinishMatch={handleFinishMatch}
          handleAwayTeamScoreGoal={handleScoreAwayGoal}
          handleHomeTeamScoreGoal={handleScoreHomeGoal}
        />
      ))}
    </div>
  );
};
