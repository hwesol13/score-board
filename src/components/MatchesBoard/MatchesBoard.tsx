import { useState, useMemo } from 'react';

import { generateMatches, generateMatch, sortMatchesByTotalScore } from '../../utils/matches/matches';
import { MatchSide } from '../../types/matches';
import { DEFAULT_MATCHES_COUNT } from '../../constants/matches';
import { BoardList } from '../BoardList';

import { MATCHES_BOARD_CONTAINER_TEST_ID, MATCHES_BOARD_START_MATCH_BUTTON_TEST_ID } from './testIds';

type Props = {
  initialMatchesCount?: number;
}

export const MatchesBoard = ({ initialMatchesCount = DEFAULT_MATCHES_COUNT }: Props) => {
  const [matches, setMatches] = useState(generateMatches(initialMatchesCount));
  const sortedMatches = useMemo(() => sortMatchesByTotalScore(matches), [matches]);

  const handleStartMatch = () => {
    setMatches([ ...matches, generateMatch() ]);
  };

  const handleFinishMatch = (matchId: string) => {
    const updatedMatches = matches.filter(({ id }) => id !== matchId);

    setMatches(updatedMatches);
  };

  const handleScoreGoal = (side: MatchSide, matchId: string) => {
    const updatedMatches = matches.map(match => {
      if (matchId === match.id) {
        match[side].score = match[side].score + 1;
      }

      return match;
    });

    setMatches(updatedMatches);
  };

  return (
    <div data-testid={MATCHES_BOARD_CONTAINER_TEST_ID}>
      <BoardList
        matches={sortedMatches}
        handleFinishMatch={handleFinishMatch}
        handleScoreGoal={handleScoreGoal}
      />
      <button
        data-testid={MATCHES_BOARD_START_MATCH_BUTTON_TEST_ID}
        onClick={handleStartMatch}
      >
        Start new match
      </button>
    </div>
  );
};
