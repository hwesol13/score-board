import { fireEvent, render, screen, within } from '@testing-library/react';

import { DEFAULT_MATCHES_COUNT } from '../../constants/matches';
import { LIST_CONTAINER_TEST_ID } from '../BoardList/testIds';
import {
  LIST_ITEM_FINISH_MATCH_BUTTON_TEST_ID,
  LIST_ITEM_HOME_SCORE_TEST_ID,
  LIST_ITEM_AWAY_SCORE_TEST_ID,
  LIST_ITEM_SCORE_HOME_GOAL_BUTTON_TEST_ID,
  LIST_ITEM_SCORE_AWAY_GOAL_BUTTON_TEST_ID, LIST_ITEM_CONTAINER_TEST_ID,
} from '../BoardListItem/testIds';

import { MatchesBoard } from './MatchesBoard';
import { MATCHES_BOARD_START_MATCH_BUTTON_TEST_ID } from './testIds';

describe('Component::MatchesBoard', () => {
  describe('Renders with default data', () => {
    beforeEach(() => {
      render(
        <MatchesBoard />
      )
    });

    it('Renders default matches element count', () => {
      const listItemElement = screen.getByTestId(LIST_CONTAINER_TEST_ID);

      expect(listItemElement).toBeInTheDocument();
      expect(listItemElement.childElementCount).toEqual(DEFAULT_MATCHES_COUNT);
    });
  })

  describe('Renders with given data', () => {
    const initialMatchesCount = 5;

    beforeEach(() => {
      render(
        <MatchesBoard initialMatchesCount={initialMatchesCount} />
      )
    });

    it('Renders given matches element count', () => {
      const listElement = screen.getByTestId(LIST_CONTAINER_TEST_ID);

      expect(listElement).toBeInTheDocument();
      expect(listElement.childElementCount).toEqual(initialMatchesCount);
    });

    it('Renders start match button', () => {
      const element = screen.getByTestId(MATCHES_BOARD_START_MATCH_BUTTON_TEST_ID);

      expect(element).toBeInTheDocument();
    });

    it('Renders started game', () => {
      const startButtonElement = screen.getByTestId(MATCHES_BOARD_START_MATCH_BUTTON_TEST_ID);
      const listElement = screen.getByTestId(LIST_CONTAINER_TEST_ID);
      const expectedMatchesCount = initialMatchesCount + 1;

      fireEvent.click(startButtonElement);

      expect(listElement).toBeInTheDocument();
      expect(listElement.childElementCount).toEqual(expectedMatchesCount);
    });

    it('Does not render finished game', () => {
      const [ finishButtonElement ] = screen.queryAllByTestId(LIST_ITEM_FINISH_MATCH_BUTTON_TEST_ID);
      const listElement = screen.getByTestId(LIST_CONTAINER_TEST_ID);
      const expectedMatchesCount = initialMatchesCount - 1;

      fireEvent.click(finishButtonElement);

      expect(listElement).toBeInTheDocument();
      expect(listElement.childElementCount).toEqual(expectedMatchesCount);
    });

    it('Renders updated home team game data when home scores a goal', () => {
      const [ scoreHomeLabelElement ] = screen.queryAllByTestId(LIST_ITEM_HOME_SCORE_TEST_ID);
      const [ scoreGoalHomeButtonElement ] = screen.queryAllByTestId(LIST_ITEM_SCORE_HOME_GOAL_BUTTON_TEST_ID);
      const currentHomeScore = Number(scoreHomeLabelElement.children);
      const expectedHomeScore = currentHomeScore + 1;

      fireEvent.click(scoreGoalHomeButtonElement);

      expect(currentHomeScore).toEqual(expectedHomeScore);
    });

    it('Renders not updated home team game data when away scores a goal', () => {
      const [ scoreHomeLabelElement ] = screen.queryAllByTestId(LIST_ITEM_HOME_SCORE_TEST_ID);
      const [ scoreGoalAwayButtonElement ] = screen.queryAllByTestId(LIST_ITEM_SCORE_AWAY_GOAL_BUTTON_TEST_ID);
      const currentHomeScore = Number(scoreHomeLabelElement.children);

      fireEvent.click(scoreGoalAwayButtonElement);

      expect(currentHomeScore).toEqual(currentHomeScore);
    });

    it('Renders updated away team game data when away scores a goal', async() => {
      const [ listItemElement ] = screen.queryAllByTestId(LIST_ITEM_CONTAINER_TEST_ID);
      const scoreAwayLabelElement = within(listItemElement).getByTestId(LIST_ITEM_AWAY_SCORE_TEST_ID);
      const scoreGoalAwayButtonElement = within(listItemElement).getByTestId(LIST_ITEM_SCORE_AWAY_GOAL_BUTTON_TEST_ID);
      const currentAwayScore = Number(scoreAwayLabelElement.children);
      const expectedAwayScore = currentAwayScore + 1;

      fireEvent.click(scoreGoalAwayButtonElement);

      expect(currentAwayScore).toEqual(expectedAwayScore);
    });

    it('Renders not updated away team game data when away scores a goal', () => {
      const [ scoreAwayLabelElement ] = screen.queryAllByTestId(LIST_ITEM_AWAY_SCORE_TEST_ID);
      const [ scoreGoalHomeButtonElement ] = screen.queryAllByTestId(LIST_ITEM_SCORE_HOME_GOAL_BUTTON_TEST_ID);
      const currentAwayScore = Number(scoreAwayLabelElement.children);

      fireEvent.click(scoreGoalHomeButtonElement);

      expect(currentAwayScore).toEqual(currentAwayScore);
    });
  })
});
