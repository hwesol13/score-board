export const LIST_ITEM_CONTAINER_TEST_ID = 'listItemContainerTestId';
export const LIST_ITEM_HOME_NAME_TEST_ID = 'listItemHomeNameLabel';
export const LIST_ITEM_AWAY_NAME_TEST_ID = 'listItemAwayNameLabel';
export const LIST_ITEM_HOME_SCORE_TEST_ID = 'listItemHomeScoreLabel';
export const LIST_ITEM_AWAY_SCORE_TEST_ID = 'listItemAwayScoreLabel';
export const LIST_ITEM_FINISH_MATCH_BUTTON_TEST_ID = 'listItemFinishMatchButton';
export const LIST_ITEM_SCORE_HOME_GOAL_BUTTON_TEST_ID = 'listItemScoreHomeGoalButton';
export const LIST_ITEM_SCORE_AWAY_GOAL_BUTTON_TEST_ID = 'listItemScoreAwayGoalButton';
