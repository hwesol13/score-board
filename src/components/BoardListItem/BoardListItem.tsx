import { DEFAULT_TEAM_SCORE } from '../../constants/matches';

import {
  LIST_ITEM_AWAY_NAME_TEST_ID,
  LIST_ITEM_AWAY_SCORE_TEST_ID,
  LIST_ITEM_HOME_NAME_TEST_ID,
  LIST_ITEM_HOME_SCORE_TEST_ID,
  LIST_ITEM_CONTAINER_TEST_ID,
  LIST_ITEM_FINISH_MATCH_BUTTON_TEST_ID,
  LIST_ITEM_SCORE_HOME_GOAL_BUTTON_TEST_ID,
  LIST_ITEM_SCORE_AWAY_GOAL_BUTTON_TEST_ID,
} from './testIds';
import './BoardListItemStyles.css';

type Props = {
  homeTeamName: string;
  awayTeamName: string;
  homeTeamScore?: number;
  awayTeamScore?: number;
  matchId: string;
  handleFinishMatch?: (matchId: string) => void;
  handleHomeTeamScoreGoal?: (matchId: string) => void;
  handleAwayTeamScoreGoal?: (matchId: string) => void;
};

export const BoardListItem = ({
  homeTeamName, homeTeamScore = DEFAULT_TEAM_SCORE, awayTeamName, awayTeamScore = DEFAULT_TEAM_SCORE, matchId,
  handleFinishMatch, handleAwayTeamScoreGoal, handleHomeTeamScoreGoal,
}: Props) => {
  return (
    <div className="list-item-container" data-testid={LIST_ITEM_CONTAINER_TEST_ID}>
      {handleHomeTeamScoreGoal ? (
        <button
          data-testid={LIST_ITEM_SCORE_HOME_GOAL_BUTTON_TEST_ID}
          onClick={() => handleHomeTeamScoreGoal(matchId)}
        >
          Home scores
        </button>
      ) : null}

      <span
        className="list-item-team-name"
        data-testid={LIST_ITEM_HOME_NAME_TEST_ID}
      >
        {homeTeamName}
      </span>
      <strong data-testid={LIST_ITEM_HOME_SCORE_TEST_ID}>{homeTeamScore}</strong>
      <strong> : </strong>
      <strong data-testid={LIST_ITEM_AWAY_SCORE_TEST_ID}>{awayTeamScore}</strong>
      <span
        className="list-item-team-name"
        data-testid={LIST_ITEM_AWAY_NAME_TEST_ID}
      >
        {awayTeamName}
      </span>

      {handleAwayTeamScoreGoal ? (
        <button
          data-testid={LIST_ITEM_SCORE_AWAY_GOAL_BUTTON_TEST_ID}
          onClick={() => handleAwayTeamScoreGoal(matchId)}
        >
          Away scores
        </button>
      ) : null}

      {handleFinishMatch ? (
        <div>
          <button
            data-testid={LIST_ITEM_FINISH_MATCH_BUTTON_TEST_ID}
            onClick={() => handleFinishMatch(matchId)}
          >
            Finish match
          </button>
        </div>
      ) : null}
    </div>
  );
};
