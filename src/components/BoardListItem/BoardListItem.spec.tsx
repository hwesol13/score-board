import { render, screen, fireEvent } from '@testing-library/react';

import { DEFAULT_TEAM_SCORE } from '../../constants/matches';

import { BoardListItem } from './BoardListItem';
import {
  LIST_ITEM_AWAY_NAME_TEST_ID,
  LIST_ITEM_AWAY_SCORE_TEST_ID,
  LIST_ITEM_FINISH_MATCH_BUTTON_TEST_ID,
  LIST_ITEM_HOME_NAME_TEST_ID,
  LIST_ITEM_HOME_SCORE_TEST_ID,
} from './testIds';

describe('Component::BoardListItem', () => {
  describe('Renders with default data', () => {
    const homeTeamName = 'testHomeName';
    const awayTeamName = 'testAwayName';
    const matchId = 'testId';

    beforeEach(() => {
      render(
        <BoardListItem
          matchId={matchId}
          homeTeamName={homeTeamName}
          awayTeamName={awayTeamName}
        />
      )
    });

    it('Renders home team score', () => {
      const element = screen.getByTestId(LIST_ITEM_HOME_SCORE_TEST_ID);

      expect(element).toBeInTheDocument();
      expect(element.innerHTML).toEqual(DEFAULT_TEAM_SCORE.toString());
    });

    it('Renders away team score', () => {
      const element = screen.getByTestId(LIST_ITEM_AWAY_SCORE_TEST_ID);

      expect(element).toBeInTheDocument();
      expect(element.innerHTML).toEqual(DEFAULT_TEAM_SCORE.toString());
    });

    it('Does not render finish match button', async () => {
      const element = await screen.queryByTestId(LIST_ITEM_FINISH_MATCH_BUTTON_TEST_ID);

      expect(element).not.toBeInTheDocument();
    });
  });

  describe('Renders with data', () => {
    const homeTeamName = 'testHomeName';
    const awayTeamName = 'testAwayName';
    const homeTeamScore = 3;
    const awayTeamScore = 8;
    const matchId = 'testId';
    const finishGameHandler = jest.fn();

    beforeEach(() => {
      render(
        <BoardListItem
          matchId={matchId}
          homeTeamName={homeTeamName}
          awayTeamName={awayTeamName}
          homeTeamScore={homeTeamScore}
          awayTeamScore={awayTeamScore}
          handleFinishMatch={finishGameHandler}
        />
      )
    });

    it('Renders home team name', async () => {
      const element = await screen.getByTestId(LIST_ITEM_HOME_NAME_TEST_ID);

      expect(element).toBeInTheDocument();
      expect(element.innerHTML).toEqual(homeTeamName);
    });

    it('Renders away team name', () => {
      const element = screen.getByTestId(LIST_ITEM_AWAY_NAME_TEST_ID);

      expect(element).toBeInTheDocument();
      expect(element.innerHTML).toEqual(awayTeamName);
    });

    it('Renders home team score', () => {
      const element = screen.getByTestId(LIST_ITEM_HOME_SCORE_TEST_ID);

      expect(element).toBeInTheDocument();
      expect(element.innerHTML).toEqual(homeTeamScore.toString());
    });

    it('Renders away team score', () => {
      const element = screen.getByTestId(LIST_ITEM_AWAY_SCORE_TEST_ID);

      expect(element).toBeInTheDocument();
      expect(element.innerHTML).toEqual(awayTeamScore.toString());
    });

    it('Renders finish match button', () => {
      const element = screen.getByTestId(LIST_ITEM_FINISH_MATCH_BUTTON_TEST_ID);

      expect(element).toBeInTheDocument();
    });

    it('Calls finish match button handler', () => {
      const element = screen.getByTestId(LIST_ITEM_FINISH_MATCH_BUTTON_TEST_ID);

      fireEvent.click(element);

      expect(finishGameHandler).toHaveBeenCalledTimes(1);
    });
  });
});
