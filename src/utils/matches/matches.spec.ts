import { DEFAULT_MATCHES_COUNT, MIN_TEAM_NAME_CHARS_COUNT } from '../../constants/matches';
import { Match } from '../../types/matches';

import {
  generateMatch,
  generateMatches,
  getRandomString,
  generateTeam, sortMatchesByTotalScore,
} from './matches';

describe('Utils: Matches', () => {
  it('Generates single match with id', () => {
    const { id }: Match = generateMatch();

    expect(id).toBeDefined();
    expect(typeof id).toEqual('string');
  });

  it('Generates single match home team data', () => {
    const { home }: Match = generateMatch();

    expect(home).toBeDefined();
    expect(home.score).toEqual(0);
    expect(typeof home.name).toEqual('string');
  });

  it('Generates single match away team data', () => {
    const { away }: Match = generateMatch();

    expect(away).toBeDefined();
    expect(away.score).toEqual(0);
    expect(typeof away.name).toEqual('string');
  });

  it('Generates default matches count', () => {
    const matches: Match[] = generateMatches();

    expect(matches.length).toEqual(DEFAULT_MATCHES_COUNT);
  });

  it('Generates given matches count', () => {
    const testMatchesCount = DEFAULT_MATCHES_COUNT + 1;
    const matches: Match[] = generateMatches(testMatchesCount);

    expect(matches.length).toEqual(testMatchesCount);
  });

  it('Generates team', () => {
    const team = generateTeam();

    expect(typeof team.score).toEqual('number');
    expect(team.score).toEqual(0);
    expect(typeof team.name).toEqual('string');
  });

  it('Generates team name', () => {
    const teamName = getRandomString();

    expect(typeof teamName).toEqual('string');
  });

  it('Generates team name with default chars count', () => {
    const teamName = getRandomString();

    expect(teamName.length).toEqual(MIN_TEAM_NAME_CHARS_COUNT);
  });

  it('Generates team name with given chars count', () => {
    const testCharsCount = MIN_TEAM_NAME_CHARS_COUNT + 1;
    const teamName = getRandomString(testCharsCount);

    expect(teamName.length).toEqual(testCharsCount);
  });

  it('Generates team name minimum chars count', () => {
    const teamName = getRandomString(0);

    expect(teamName.length).toEqual(MIN_TEAM_NAME_CHARS_COUNT);
  });

  it('Sorts matches', () => {
    const matches: Match[] = generateMatches();
    const highestTotalMatchIndex = DEFAULT_MATCHES_COUNT - 1;
    const highestTotalMatchId = matches[highestTotalMatchIndex].id;

    matches[highestTotalMatchIndex].home.score = 9;
    matches[highestTotalMatchIndex].away.score = 9;

    const sortedMatches = sortMatchesByTotalScore(matches);
    const highestTotalSortedMatchId = sortedMatches[0].id;

    expect(highestTotalMatchId).toEqual(highestTotalSortedMatchId);
  });
});

