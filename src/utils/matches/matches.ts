import { Match, Team } from '../../types/matches';
import { DEFAULT_MATCHES_COUNT, MIN_TEAM_NAME_CHARS_COUNT } from '../../constants/matches';

export const getRandomString = (charsCount: number = MIN_TEAM_NAME_CHARS_COUNT): string => {
  const teamNameCharsCount = Math.max(charsCount, MIN_TEAM_NAME_CHARS_COUNT);

  return Math.random().toString(36).substring(0, teamNameCharsCount);
};

export const generateTeam = (teamNameCharsCount: number = MIN_TEAM_NAME_CHARS_COUNT): Team => {
  return {
    name: getRandomString(teamNameCharsCount),
    score: 0,
  };
};

export const generateMatch = (): Match => {
  return {
    id: getRandomString(),
    home: generateTeam(),
    away: generateTeam(),
  }
};

export const generateMatches = (matchesCount: number = DEFAULT_MATCHES_COUNT): Match[] => {
  return [ ...Array(matchesCount) ].map(generateMatch);
};

export const sortMatchesByTotalScore = (matches: Match[]): Match[] => {
  return matches.sort((matchA, matchB) => {
    const matchATotal = matchA.home.score + matchA.away.score;
    const matchBTotal = matchB.home.score + matchB.away.score;

    return (matchATotal < matchBTotal) ? 1 : -1;
  });
};
