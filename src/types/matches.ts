export enum MatchSide {
  HOME = 'home',
  AWAY = 'away',
}

export type Team = {
  name: string;
  score: number;
}

export type Match = {
  id: string;
  [MatchSide.HOME]: Team;
  [MatchSide.AWAY]: Team;
}
