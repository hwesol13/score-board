import React from 'react';
import './App.css';
import { MatchesBoard } from './components/MatchesBoard';

const App = () => {
  return (
    <div className="App">
      <MatchesBoard />
    </div>
  );
}

export default App;
